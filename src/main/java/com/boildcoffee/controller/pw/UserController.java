package com.boildcoffee.controller.pw;

import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * Created by jason on 2017/11/10.
 */
@RestController
public class UserController extends BaseController{

    @RequestMapping("/hello")
    @ResponseBody
    public String hello(){
        return "hello";
    }

    @RequestMapping(value = "/user/login",method = RequestMethod.POST)
    @ResponseBody
    public String login(){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("s",1);
        JSONObject jsonObject1 = new JSONObject();
        jsonObject1.put("token", UUID.randomUUID());
        jsonObject1.put("user_id",1);
        jsonObject1.put("state",1);
        jsonObject.put("d",jsonObject1);
        return jsonObject.toString();
    }

    @RequestMapping(value = "/user/register",method = RequestMethod.POST)
    @ResponseBody
    public String register(){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("s",1);
        jsonObject.put("d",new JSONObject());
        return jsonObject.toString();
    }

    @RequestMapping(value = "/smsCode",method = RequestMethod.GET)
    @ResponseBody
    public String getSmsCode(){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("s",1);
        jsonObject.put("d",new JSONObject());
        return jsonObject.toString();
    }

}
